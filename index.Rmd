---
layout: post
title: "Conference poster: GradSAM 2012"
date: 2012-02-08T18:00:00+01:00
slug: "gradsam-poster"
tags:
- poster
- GradSAM21
draft: no
sourcerepo: "https://codeberg.org/solarchemist/2012-02-08-gradsam-poster"
---


```{r packages, echo=FALSE, results='hide', message=FALSE}
library(knitr)
```

```{r global_options, echo=FALSE, results='hide', message=FALSE}
options(digits   = 7,
        width    = 84,
        continue = " ",
        prompt   = "> ",
        warn = 0,
        stringsAsFactors = FALSE)
opts_chunk$set(
   dev        = 'svg',
   out.width  = "100%",
   fig.align  = 'center',
   echo       = TRUE,
   eval       = TRUE,
   cache      = TRUE,
   collapse   = TRUE,
   results    = 'hide',
   message    = FALSE,
   warning    = FALSE,
   tidy       = FALSE)
```




I attended the [GradSAM 2012 conference](http://www.avanceradematerial.fysik.uu.se/workshop2012.html), which was organised by the [Graduate School in Advanced Materials for the 21st century](http://www.avanceradematerial.fysik.uu.se/gradschool.html) at the faculty of Science and technology, Uppsala university (a local conference primarily for the doctoral students at the faculty).

We presented the poster "Electrodeposited zinc oxide nanorods coated with atomic-layer deposited iron oxide", which included some X-ray flyuorescence, SEM, and optical UV/Vis characterisation data of $\ce{ZnO}/\ce{Fe2O3}$ nanorod arrays.

I made the poster using LaTeX (with the `beamerposter` package), Sweave and R.



![](assets/120208-poster.png)


<a href="https://dx.doi.org/10.6084/m9.figshare.3569655.v1">
<div style="border: 2px solid blue; padding-left: 6px; padding-top: 2px; background-color: #EBEBEB; color: black; text-align: center;">
The poster and its source files are posted on Figshare.
<br>
https://dx.doi.org/10.6084/m9.figshare.3569655.v1
</div>
</a>




## Tools

- [beamerposter Github page](https://github.com/deselaers/latex-beamerposter)
- [beamerposter on CTAN](http://www.ctan.org/pkg/beamerposter)
- [beamerposter Google Group](https://groups.google.com/forum/#!forum/beamerposter)
- [Sweave](https://www.statistik.lmu.de/~leisch/Sweave/)
- [TikZ](https://en.wikibooks.org/wiki/LaTeX/PGF/TikZ)








## Session info

```{r echo=FALSE, results='markup', cache=F}
sessionInfo()
```
